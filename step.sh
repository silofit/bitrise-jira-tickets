#!/bin/bash

#set -ex

echo "Reference branch: ${reference_branch}"
echo "Fetching tickets from selected branch: ${selected_branch}"
echo "JIRA ticket URL path: ${url_path}"
echo "JIRA ticket prefix: ${prefix}"

ESCAPED_URL=$(echo ${url_path} | sed -e "s#/#\\\/#g")
JIRA_tickets_string=""
JIRA_tickets=($(git log --no-merges --pretty=format:"%s" ${reference_branch}..${selected_branch} | grep "${prefix}-[0-9]{1,5}" -o -E | sort -u -r --version-sort | sed -e 's/^/'${ESCAPED_URL}'/'))

for (( i=0 ; i<${#JIRA_tickets[*]} ; ++i ))
do
    JIRA_tickets_string+="${JIRA_tickets[$i]}"

    if (( i < ${#JIRA_tickets[*]} - 1 ))
    then
        JIRA_tickets_string+=$'\n'
    fi
done

#Update ENV VARIABLE on Bitrise
echo "-------------------"
echo "JIRA tickets found: "
echo "${JIRA_tickets_string}"
envman add --key JIRA_TICKET_ID_LIST --value "$JIRA_tickets_string"
